<?php

define('__APP__', __DIR__ . '/../app');

require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require __APP__ . '/settings.php';

$app = new \Slim\App($settings);

// Set up dependencies
require __APP__ . '/dependencies.php';

// Register routes
require __APP__ . '/routes.php';

// Run app
$app->run();
