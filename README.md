[![Build Status](https://travis-ci.org/oxkhar/mums.svg?branch=master)](https://travis-ci.org/oxkhar/mums)

# Mum's Shop

Gestión de los pedidos de comida casera a Mum's

## Requerimientos

* **PHP 5.6** en su versión de linea de comandos (php5-cli)
* Los siguieentes módulos.
  * sqlite
* [Composer](https://getcomposer.org/)

## Instalación

### Automática

```
./build.sh
```

### Manual

* **PHP**: Para sistemas basados en Debian (Ubuntu,...)

```
apt-get install php5-cli php5-sqlite
```

* **Composer**

```
php -r 'readfile("https://getcomposer.org/installer");' |  php -- --install-dir=bin --version=1.1.2 --filename=composer
```

## Despliegue

```
bin/composer install
```

## Ejecución

```
php -S localhost:8080 -t www www/index.php
```

### Aplicación Web
Accede con un navegador web a ...
* http://localhost:8080/meals

## Tests

* Ejecución de los tests **de especificaciones**...

```
bin/composer tests

# O lo que es lo mismo
bin/phpspec run --format=pretty
```

> Informe de cobertura de tests en ```build/coverage/index.html``` (sólo si esta activo el módulo de **xdebug**)

