<?php

namespace Mums\Shop\Exception;

use Mums\Shop\Model\Meal;

class InvalidMealsException extends \InvalidArgumentException
{

    public function __construct($meals = "")
    {
        parent::__construct(
            "Meals '" .
            (is_array($meals) ? implode(", ", $meals) : $meals) .
            "' are not valid"
        );
    }
}
