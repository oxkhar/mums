<?php

namespace Mums\Shop\Manager\Calculator;

use Mums\Shop\Model\MealType;
use Mums\Shop\Model\NormalPrice;
use Mums\Shop\Model\Offer3x2;
use Mums\Shop\Model\OfferMenu;

abstract class PriceCalculator
{

    private $nextCalculator;
    
    protected $prices = [];

    public function __construct(PriceCalculator $nextCalculator = null)
    {
        $this->nextCalculator = $nextCalculator;
    }

    abstract protected function generatePrices(array $meals);

    public function calculatePrices(array $meals)
    {
        $this->nextCalculator($this->generatePrices($meals));

        return $this;
    }

    private function nextCalculator($meals)
    {
        if (isset($this->nextCalculator)) {
            $this->nextCalculator->calculatePrices($meals);
        }
    }

    protected function setPrices(array $prices)
    {
        $this->prices = $prices;
    }

    public function getPrices()
    {
        return array_merge(
            $this->prices,
            (isset($this->nextCalculator) ? $this->nextCalculator->getPrices() : [])
        );
    }
}
