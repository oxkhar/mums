<?php

namespace Mums\Shop\Manager\Calculator;

use Mums\Shop\Model\GroupPrice;
use Mums\Shop\Model\Meal;

class GroupPriceCalculator extends PriceCalculator
{
    protected function generatePrices(array $meals)
    {
        if (empty($meals)) {
            return $meals;
        }

        $group = array_reduce(
            $meals,
            function (array $group, Meal $meal) {
                $group[$meal->id()][] = $meal;
                return $group;
            },
            []
        );

        $this->prices = array_map(
            function ($elm) {
                return new GroupPrice($elm);
            },
            array_values($group)
        );

        // we process all meals, and don't let any one pending price
        return [];
    }
}
