<?php

namespace Mums\Shop\Manager\Calculator;

use Mums\Shop\Model\Meal;
use Mums\Shop\Model\Offer3x2;

class Offer3x2Calculator extends PriceCalculator
{
    protected function generatePrices(array $meals)
    {
        if (count($meals) < 3) {
            return $meals;
        }
        
        $mealsEquals = [];
        $prices = [];

        foreach ($meals as $meal) {
            $id = $meal->id();
            $mealsEquals[$id][] = $meal;

            if ($this->canGenerateOffer($mealsEquals[$id])) {
                $prices[] = new Offer3x2($mealsEquals[$id]);
                unset($mealsEquals[$id]);
            }
        }
            
        $this->setPrices($prices);

        // return rest meals that can not be a offer
        return array_reduce($mealsEquals, 'array_merge', []);
    }

    private function canGenerateOffer(array $meals)
    {
        return count($meals) === 3;
    }
}
