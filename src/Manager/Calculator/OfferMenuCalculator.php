<?php

namespace Mums\Shop\Manager\Calculator;

use Mums\Shop\Model\Meal;
use Mums\Shop\Model\MealType;
use Mums\Shop\Model\OfferMenu;

class OfferMenuCalculator extends PriceCalculator
{

    protected function generatePrices(array $meals)
    {
        if (count($meals) < 3) {
            return $meals;
        }

        $mealsType = [];
        $prices = [];

        foreach ($meals as $meal) {
            $mealsType[$meal->type()][] = $meal;

            if ($this->canGenerateOffer($mealsType)) {
                $prices[] = new OfferMenu(
                    array_shift($mealsType[MealType::MAIN]),
                    array_shift($mealsType[MealType::DESSERT]),
                    array_shift($mealsType[MealType::DRINK])
                );
            }
        }
        
        $this->setPrices($prices);

        // return rest meals that can not be a offer
        return array_reduce($mealsType, 'array_merge', []);
    }

    private function canGenerateOffer(array $mealsType)
    {
        return (!empty($mealsType[MealType::MAIN]) &&
            !empty($mealsType[MealType::DESSERT]) &&
            !empty($mealsType[MealType::DRINK])
            );
    }
}
