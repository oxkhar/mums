<?php

namespace Mums\Shop\Manager;

use Mums\Shop\Repository\MealRepository;

class MealsManager
{

    private $repository;

    public function __construct(MealRepository $repository)
    {
        $this->repository = $repository;
    }

    public function giveMeAll()
    {
        return $this->repository->findAll();
    }

    public function find(array $listIds)
    {
        return $this->repository->findByIds($listIds);
    }
}
