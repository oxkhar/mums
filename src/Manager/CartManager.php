<?php

namespace Mums\Shop\Manager;

use Mums\Shop\Model\Cart;
use Mums\Shop\Model\UUID;

class CartManager
{

    private $repository;

    function __construct(\Mums\Shop\Repository\CartRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create()
    {
        return new Cart(UUID::generate());
    }

    public function store(Cart $cart)
    {
        $this->repository->save($cart);
    }

    public function recover($id)
    {
        return $this->repository->findById($id);
    }
}
