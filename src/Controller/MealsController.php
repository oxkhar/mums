<?php

namespace Mums\Shop\Controller;

class MealsController extends BaseController
{

    public function meals(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $args = [
            'meals' => $this->ci->get('meals-manager')->giveMeAll()
        ];

        return $this->renderer->render($response, 'meals.phtml', $args);
    }
}
