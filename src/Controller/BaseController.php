<?php

namespace Mums\Shop\Controller;

use Interop\Container\ContainerInterface;

class BaseController
{

    protected $ci;

    protected $renderer;

    //Constructor
    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;

        $this->renderer = $this->ci->get('renderer');
    }
}
