<?php

namespace Mums\Shop\Model;

class Meal
{

    private $id;
    private $name;
    private $price;
    private $type;

    public function __construct($id, $name, $price, $type)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->price = $price;
        $this->type  = $type;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function price()
    {
        return $this->price;
    }

    public function type()
    {
        return $this->type;
    }
}
