<?php

namespace Mums\Shop\Model;

class MealType
{

    const MAIN = "main";

    const DRINK = "drink";

    const DESSERT = "dessert";
}
