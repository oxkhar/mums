<?php

namespace Mums\Shop\Model;

interface Offer
{
    public function discount();
    public function total();
}
