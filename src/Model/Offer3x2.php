<?php

namespace Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;

class Offer3x2 implements Price, Offer
{

    private $meals;
    private $amount;

    public function __construct(array $meals)
    {
        if (count($meals) != 3) {
            throw new InvalidMealsException();
        }

        if (!$this->mealsAreValid($meals)) {
            throw new InvalidMealsException(array_map(
                function (Meal $meal) {
                    return $meal->id();
                },
                $meals
            ));
        }

        $this->meals = $meals;

        $this->generateAmounts();
    }

    private function mealsAreValid($meals)
    {
        return count(array_unique(array_map(
            function (Meal $meal) {
                return $meal->id();
            },
            $meals
        ))) === 1;
    }

    private function generateAmounts()
    {
        $amount = [
            "total" => array_sum(array_map(
                function (Meal $meal) {
                    return $meal->price();
                },
                $this->meals
            ))
        ];

        $amount["discount"] = $this->meals[0]->price();

        $amount["price"] = $amount["total"] - $amount["discount"];

        $this->amount = array_map(
            function ($num) {
                return round($num, 2);
            },
            $amount
        );
    }

    public function discount()
    {
        return $this->amount["discount"];
    }

    public function total()
    {
        return $this->amount["total"];
    }

    public function price()
    {
        return $this->amount["price"];
    }

    public function description()
    {
        $desc = "Offer 3x2 : " . $this->meals[0]->name();

        return $desc;
    }

    public function elements()
    {
        return $this->meals;
    }
}
