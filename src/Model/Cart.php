<?php

namespace Mums\Shop\Model;

class Cart
{
    private $id;

    private $products;

    public function __construct($id)
    {
        $this->id = $id;

        $this->products = new \ArrayObject();
    }

    public function addMeal(Meal $meal)
    {
        $this->products->append($meal);
    }

    public function count()
    {
        return $this->products->count();
    }

    public function giveBackMeal($position)
    {
        $this->products->offsetUnset($position);
    }

    public function listMeals()
    {
        return $this->products->getArrayCopy();
    }

    public function id()
    {
        return $this->id;
    }
}
