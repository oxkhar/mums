<?php

namespace Mums\Shop\Model;

interface Price
{
    public function elements();
    public function description();
    public function price();
}
