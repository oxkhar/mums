<?php

namespace Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;

class OfferMenu implements Price, Offer
{

    const DISCOUNT = 20.0;

    private $meals;
    private $amount;

    public function __construct(Meal $main, Meal $dessert, Meal $drink)
    {
        if ($main->type() != MealType::MAIN) {
            throw new InvalidMealsException($main->type());
        }

        if ($dessert->type() != MealType::DESSERT) {
            throw new InvalidMealsException($dessert->type());
        }

        if ($drink->type() != MealType::DRINK) {
            throw new InvalidMealsException($drink->type());
        }

        $this->meals = [$main, $dessert, $drink];

        $this->generateAmounts();
    }

    private function generateAmounts()
    {
        $amount = [
            "total" => array_sum(array_map(
                function (Meal $meal) {
                    return $meal->price();
                },
                $this->meals
            ))
        ];

        $amount["discount"] = $amount["total"] * self::DISCOUNT / 100.0;

        $amount["price"] = $amount["total"] - $amount["discount"];

        $this->amount = array_map(
            function ($num) {
                return round($num, 2);
            },
            $amount
        );
    }

    public function discount()
    {
        return $this->amount["discount"];
    }

    public function total()
    {
        return $this->amount["total"];
    }

    public function price()
    {
        return $this->amount["price"];
    }

    public function description()
    {
        $desc = "Offer menu " . self::DISCOUNT . "% discount: ";
        $desc .= implode(
            ", ",
            array_map(
                function (Meal $meal) {
                    return $meal->name();
                },
                $this->elements()
            )
        );

        return $desc;
    }

    public function elements()
    {
        return $this->meals;
    }
}
