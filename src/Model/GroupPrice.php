<?php

namespace Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;

class GroupPrice implements Price
{

    private $meals;
    private $amount;

    public function __construct($meals)
    {

        if (!$this->mealsAreValid($meals)) {
            throw new InvalidMealsException(array_map(
                function (Meal $meal) {
                    return $meal->id();
                },
                $meals
            ));
        }

        $this->meals = $meals;

        $this->generateAmounts();
    }

    private function mealsAreValid($meals)
    {
        return count(array_unique(array_map(
            function (Meal $meal) {
                return $meal->id();
            },
            $meals
        ))) === 1;
    }

    private function generateAmounts()
    {
        $amount = [
            "price" => $this->meals[0]->price() * count($this->meals)
        ];

        $this->amount = array_map(
            function ($num) {
                return round($num, 2);
            },
            $amount
        );
    }

    public function price()
    {
        return $this->amount["price"];
    }

    public function description()
    {
        return $this->meals[0]->name() . " X " . count($this->meals);
    }

    public function elements()
    {
        return $this->meals;
    }
}
