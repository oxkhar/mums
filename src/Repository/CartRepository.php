<?php

namespace Mums\Shop\Repository;

use Mums\Shop\Model\Cart;

interface CartRepository
{

    public function findById($id);

    public function save(Cart $cart);
}
