<?php

namespace Mums\Shop\Repository;

interface MealRepository
{

    public function findAll();

    public function findByIds(array $ids);
}
