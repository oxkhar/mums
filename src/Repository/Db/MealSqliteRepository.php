<?php

namespace Mums\Shop\Repository\Db;

use ArrayAccess;
use Mums\Shop\Model\Meal;
use Mums\Shop\Model\UUID;
use Mums\Shop\Repository\MealRepository;
use PDO;

class MealSqliteRepository implements MealRepository
{

    private $dbm;

    public function __construct(PDO $dbm)
    {
        $this->dbm = $dbm;
    }

    public function nextId()
    {
        return UUID::generate();
    }

    public function findByIds(array $ids)
    {
        $stm = $this->dbm->query(
            "SELECT * FROM meals " .
            "WHERE uuid = '" . implode("','", $ids) . "'"
        );

        foreach ($stm as $row) {
            yield $this->newMeal($row);
        }

        $stm->closeCursor();
    }

    public function findAll()
    {
        $stm = $this->dbm->query("SELECT * FROM meals");
        foreach ($stm as $row) {
            yield $this->newMeal($row);
        }
        $stm->closeCursor();
    }

    /**
     * @param array|ArrayAccess $row
     * @return User
     */
    protected function newMeal($row)
    {
        $row['uuid'] = new UUID($row['uuid']);

        return new Meal($row['uuid'], $row['name'], $row['price'], $row['type']);
    }
}
