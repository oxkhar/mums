<?php

namespace spec\Mums\Shop\Model;

use Mums\Shop\Model\Meal;
use Mums\Shop\Model\MealType;
use PhpSpec\ObjectBehavior;

class MealSpec extends ObjectBehavior
{

    function let()
    {
        $this->beConstructedWith(
            "ID-1",
            "Apple",
            1.5,
            MealType::DESSERT
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Meal::class);
    }

    function it_return_data_formed_by_entity()
    {
        $this->beConstructedWith(
            $id = "ID-1",
            $name = "Apple",
            $price = 1.5,
            $type = MealType::DESSERT
        );

        $this->id()->shouldBe($id);
        $this->name()->shouldBe($name);
        $this->price()->shouldBe($price);
        $this->type()->shouldBe($type);
    }
}
