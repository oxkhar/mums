<?php

namespace spec\Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;
use Mums\Shop\Model\Offer;
use Mums\Shop\Model\Offer3x2;
use Mums\Shop\Model\Price;
use PhpSpec\ObjectBehavior;
use spec\Mums\Shop\Tools\MealsFactory;

class Offer3x2Spec extends ObjectBehavior
{
    function let() {
        $meal = MealsFactory::drink();
        $this->beConstructedWith([
            $meal,
            clone $meal,
            clone $meal
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Offer3x2::class);
    }

    function it_has_prices()
    {
        $this->shouldImplement(Price::class);
    }

    function it_is_a_offer()
    {
        $this->shouldImplement(Offer::class);
    }

    function it_should_not_allowed_incorrect_type_meals()
    {
        $this->beConstructedWith([
            MealsFactory::drink(),
            MealsFactory::drink(),
            MealsFactory::drink()
        ]);

        $this->shouldThrow(InvalidMealsException::class)->duringInstantiation();
    }

    function it_should_give_amounts() {
        $meal = MealsFactory::drink(1.65);
        $this->beConstructedWith([
            $meal,
            clone $meal,
            clone $meal
        ]);

        $this->price()->shouldBe(3.30);
        $this->discount()->shouldBe(1.65);
        $this->total()->shouldBe(4.95);

    }

    function it_should_show_info_about_offer()
    {
        $meal = MealsFactory::drink();
        $this->beConstructedWith([
            $meal,
            clone $meal,
            clone $meal
        ]);

        $desc = $this->description();

        $desc->shouldContain($meal->name());
    }

    function it_should_give_meals_contain_offer() {
        $meal1 = MealsFactory::drink();
        $meal2 = clone $meal1;
        $meal3 = clone $meal2;
        $this->beConstructedWith([
            $meal1,
            $meal2,
            $meal3
        ]);

        $elements = $this->elements();

        $elements->shouldContain($meal1);
        $elements->shouldContain($meal2);
        $elements->shouldContain($meal3);
    }
}
