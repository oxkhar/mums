<?php

namespace spec\Mums\Shop\Model;

use Mums\Shop\Model\Cart;
use Mums\Shop\Model\Meal;
use Mums\Shop\Model\MealType;
use PhpSpec\ObjectBehavior;

class CartSpec extends ObjectBehavior
{
    const CART_ID = "cart-1";

    function let()
    {
        $this->beConstructedWith(self::CART_ID);
    }

    private static function mealsFixtures()
    {
        return [
            new Meal("ID-1", "meal-1", 11.1, MealType::MAIN),
            new Meal("ID-2", "meal-2", 22.2, MealType::DRINK),
            new Meal("ID-3", "meal-3", 33.3, MealType::DESSERT)
        ];
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Cart::class);
    }

    function it_should_be_identified()
    {
        $this->id()->shouldBe(self::CART_ID);
    }

    function it_give_all_items_stored()
    {
        $meals = self::mealsFixtures();

        array_map([$this, 'addMeal'], $meals);

        $this->listMeals()->shouldBeEqualTo($meals);
    }

    function it_add_meals_we_want_buy()
    {
        $meals = self::mealsFixtures();

        array_map([$this, 'addMeal'], $meals);

        $this->count()->shouldBe(3);
        $this->listMeals()->shouldContain($meals[0]);
        $this->listMeals()->shouldContain($meals[1]);
        $this->listMeals()->shouldContain($meals[2]);
    }

    function it_remove_a_specific_product()
    {
        $meals = self::mealsFixtures();

        array_map([$this, 'addMeal'], $meals);

        $this->giveBackMeal(2);

        $this->count()->shouldBe(2);

        $this->listMeals()->shouldNotContain($meals[2]);
    }
}
