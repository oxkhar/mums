<?php

namespace spec\Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;
use Mums\Shop\Model\GroupPrice;
use Mums\Shop\Model\Price;
use PhpSpec\ObjectBehavior;
use spec\Mums\Shop\Tools\MealsFactory;

class GroupPriceSpec extends ObjectBehavior
{

    function let()
    {
        $meal = MealsFactory::drink();
        $this->beConstructedWith([
            $meal,
            clone $meal
        ]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(GroupPrice::class);
    }

    function it_has_prices()
    {
        $this->shouldImplement(Price::class);
    }

    function it_should_not_allow_diferent_meals()
    {
        $this->beConstructedWith([
            MealsFactory::drink(),
            MealsFactory::drink()
        ]);

        $this->shouldThrow(InvalidMealsException::class)->duringInstantiation();
    }

    function it_should_give_amounts()
    {
        $meal = MealsFactory::drink(1.95);
        $this->beConstructedWith([
            $meal,
            clone $meal,
            clone $meal
        ]);

        $this->price()->shouldBe(5.85);
    }

    function it_should_show_info_about_offer()
    {
        $meal = MealsFactory::drink();
        $this->beConstructedWith([
            $meal,
            clone $meal
        ]);

        $desc = $this->description();

        $desc->shouldContain($meal->name());
    }

    function it_should_give_meals() {
        $meal1 = MealsFactory::drink();
        $meal2 = clone $meal1;

        $this->beConstructedWith([
            $meal1,
            $meal2,
        ]);

        $elements = $this->elements();

        $elements->shouldContain($meal1);
        $elements->shouldContain($meal2);
    }

}
