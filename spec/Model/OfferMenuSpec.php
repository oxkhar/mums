<?php

namespace spec\Mums\Shop\Model;

use Mums\Shop\Exception\InvalidMealsException;
use Mums\Shop\Model\Offer;
use Mums\Shop\Model\OfferMenu;
use Mums\Shop\Model\Price;
use PhpSpec\ObjectBehavior;
use spec\Mums\Shop\Tools\MealsFactory;

class OfferMenuSpec extends ObjectBehavior
{

    function let()
    {
        $main    = MealsFactory::main();
        $dessert = MealsFactory::dessert();
        $drink   = MealsFactory::drink();

        $this->beConstructedWith($main, $dessert, $drink);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(OfferMenu::class);
    }

    function it_has_prices()
    {
        $this->shouldImplement(Price::class);
    }

    function it_is_a_offer()
    {
        $this->shouldImplement(Offer::class);
    }

    function it_should_not_allowed_incorrect_type_meals()
    {
        $main    = MealsFactory::main();
        $dessert = MealsFactory::dessert();
        $drink   = MealsFactory::drink();

        $this->beConstructedWith($drink, $dessert, $drink);
        $this->shouldThrow(InvalidMealsException::class)->duringInstantiation();

        $this->beConstructedWith($main, $drink, $drink);
        $this->shouldThrow(InvalidMealsException::class)->duringInstantiation();

        $this->beConstructedWith($main, $dessert, $main);
        $this->shouldThrow(InvalidMealsException::class)->duringInstantiation();
    }

    function it_should_give_amounts()
    {
        $main    = MealsFactory::main(15.40);
        $dessert = MealsFactory::dessert(7.25);
        $drink   = MealsFactory::drink(1.95);

        $this->beConstructedWith($main, $dessert, $drink);

        $this->price()->shouldBe(19.68);
        $this->discount()->shouldBe(4.92);
        $this->total()->shouldBe(24.60);
    }

    function it_should_show_info_about_offer()
    {
        $main    = MealsFactory::main();
        $dessert = MealsFactory::dessert();
        $drink   = MealsFactory::drink();

        $this->beConstructedWith($main, $dessert, $drink);

        $desc = $this->description();

        $desc->shouldContain($main->name());
        $desc->shouldContain($dessert->name());
        $desc->shouldContain($drink->name());
    }

    function it_should_give_meals_contain_offer() {
        $main    = MealsFactory::main();
        $dessert = MealsFactory::dessert();
        $drink   = MealsFactory::drink();

        $this->beConstructedWith($main, $dessert, $drink);

        $elements = $this->elements();
        $elements->shouldContain($main);
        $elements->shouldContain($dessert);
        $elements->shouldContain($drink);
    }
}
