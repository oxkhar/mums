<?php

namespace spec\Mums\Shop\Tools;

use Faker\Factory;
use Mums\Shop\Model\Meal;
use Mums\Shop\Model\MealType;

class MealsFactory
{

    public static function meal($type = null, $price = null)
    {
        $faker = Factory::create();

        return new Meal(
            $faker->uuid(),
            $faker->name(),
            $price ? : $faker->randomFloat(2),
            $type ? : $faker->randomElement([
                    MealType::DESSERT,
                    MealType::DRINK,
                    MealType::MAIN
                ])
        );
    }

    public static function dessert($price = null) {
        return self::meal(MealType::DESSERT, $price);
    }

    public static function main($price = null)
    {
        return self::meal(MealType::MAIN, $price);
    }

    public static function drink($price = null)
    {
        return self::meal(MealType::DRINK, $price);
    }
}
