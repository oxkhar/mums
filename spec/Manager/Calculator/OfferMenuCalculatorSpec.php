<?php

namespace spec\Mums\Shop\Manager\Calculator;

use Mums\Shop\Manager\Calculator\OfferMenuCalculator;
use Mums\Shop\Model\OfferMenu;
use spec\Mums\Shop\Tools\MealsFactory;

class OfferMenuCalculatorSpec extends PriceCalculatorBase
{

    function it_is_initializable()
    {
        $this->shouldHaveType(OfferMenuCalculator::class);
    }

    public function getValidMeals() {
        return [
            MealsFactory::drink(),
            MealsFactory::main(),
            MealsFactory::dessert()
        ];
    }

    public function getNotValidMeals() {
        return [
            MealsFactory::drink(),
            MealsFactory::dessert()
        ];
    }

    function it_generate_offers_menu()
    {
        $drink = MealsFactory::drink();
        $main  = MealsFactory::main();
        $fruit = MealsFactory::dessert();

        $meals = [
            $main,
            clone $main,
            $fruit,
            clone $main,
            clone $fruit,
            $fruit,
            $drink,
            clone $fruit,
            $drink,
            clone $fruit
        ];

        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldHaveCount(2);
        $prices[0]->shouldImplement(OfferMenu::class);
        $prices[1]->shouldImplement(OfferMenu::class);
    }

    function it_has_not_offer_without_valid_meals()
    {
        $main  = MealsFactory::main();
        $fruit = MealsFactory::dessert();

        $meals = [
            $main,
            $fruit,
            clone $main,
            clone $fruit,
            clone $fruit,
        ];

        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldHaveCount(0);
    }
}
