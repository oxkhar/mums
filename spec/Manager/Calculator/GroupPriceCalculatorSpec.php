<?php

namespace spec\Mums\Shop\Manager\Calculator;

use Mums\Shop\Manager\Calculator\GroupPriceCalculator;
use Mums\Shop\Model\GroupPrice;
use spec\Mums\Shop\Tools\MealsFactory;

class GroupPriceCalculatorSpec extends PriceCalculatorBase
{
    function it_is_initializable()
    {
        $this->shouldHaveType(GroupPriceCalculator::class);
    }


    public function getNotValidMeals()
    {
        return [];
    }

    public function getValidMeals()
    {
        $meal = MealsFactory::drink();
        return [
            $meal,
            clone $meal
        ];
    }

    function it_generate_groups_for_meals_of_same_type() {
        $meal = MealsFactory::drink();
        $meals = [
            MealsFactory::drink(),
            MealsFactory::main(),
            $meal,
            MealsFactory::dessert(),
            clone $meal
        ];


        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldHaveCount(4);
        $prices[0]->shouldImplement(GroupPrice::class);
        $prices[1]->shouldImplement(GroupPrice::class);
        $prices[2]->shouldImplement(GroupPrice::class);
        $prices[3]->shouldImplement(GroupPrice::class);
    }

}
