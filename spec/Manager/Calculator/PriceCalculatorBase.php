<?php

namespace spec\Mums\Shop\Manager\Calculator;

use Mums\Shop\Manager\Calculator\PriceCalculator;
use Mums\Shop\Model\Offer3x2;
use Mums\Shop\Model\Price;
use PhpSpec\ObjectBehavior;
use spec\Mums\Shop\Tools\MealsFactory;

abstract class PriceCalculatorBase extends ObjectBehavior
{
    function it_is_price_calculator() {
        $this->shouldImplement(PriceCalculator::class);
    }

    abstract function getNotValidMeals();
    abstract function getValidMeals();

    function it_get_prices_only_when_calculate_prices_on_meals() {
        
        $meals = $this->getValidMeals();

        $this->getPrices()->shouldHaveCount(0);

        $this->calculatePrices($meals);

        $this->getPrices()->shouldHaveCount(1);
    }

    function it_chains_other_price_calculator_with_rest_meals(PriceCalculator $calculator)
    {
        $this->beConstructedWith($calculator);

        $expected = $this->getNotValidMeals();
        $meals = array_merge($this->getValidMeals(), $expected);

        $prices = $this->calculatePrices($meals);

        $calculator->calculatePrices($expected)->shouldHaveBeenCalled();
    }

    function it_add_prices_when_chains_with_other_price_calculator(
        PriceCalculator $calculator,
        Price $price
    )
    {
        $this->beConstructedWith($calculator);

        $expected = $this->getNotValidMeals();
        $meals = array_merge($this->getValidMeals(), $expected);

        $calculator->calculatePrices($expected)->willReturn([]);
        $calculator->getPrices()->willReturn([$price]);

        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldContain($price);
    }
}
