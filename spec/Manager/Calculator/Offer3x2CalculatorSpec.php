<?php

namespace spec\Mums\Shop\Manager\Calculator;

use Mums\Shop\Manager\Calculator\Offer3x2Calculator;
use Mums\Shop\Manager\Calculator\PriceCalculator;
use Mums\Shop\Model\Offer3x2;
use Mums\Shop\Model\Price;
use PhpSpec\ObjectBehavior;
use spec\Mums\Shop\Tools\MealsFactory;

class Offer3x2CalculatorSpec extends PriceCalculatorBase
{

    function it_is_initializable()
    {
        $this->shouldHaveType(Offer3x2Calculator::class);
    }

    function getValidMeals()
    {
        $meal = MealsFactory::drink();
        return [
            clone $meal,
            clone $meal,
            clone $meal
        ];
    }

    function getNotValidMeals()
    {
        return [
            MealsFactory::drink(),
            MealsFactory::drink(),
            MealsFactory::drink()
        ];
    }

    function it_generate_offers_3x2()
    {
        $meal  = MealsFactory::drink();
        $fruit = MealsFactory::dessert();

        $meals = [
            $meal,
            clone $meal,
            $fruit,
            clone $meal,
            clone $fruit,
            clone $fruit
        ];

        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldHaveCount(2);
        $prices[0]->shouldImplement(Offer3x2::class);
        $prices[1]->shouldImplement(Offer3x2::class);
    }

    function it_has_not_offer_without_valid_meals()
    {
        $meal  = MealsFactory::drink();
        $meals = [
            $meal,
            clone $meal,
            MealsFactory::drink(),
            MealsFactory::drink(),
            MealsFactory::drink()
        ];

        $prices = $this->calculatePrices($meals)->getPrices();

        $prices->shouldHaveCount(0);
    }
}
