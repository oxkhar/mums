<?php

namespace spec\Mums\Shop\Manager;

use Mums\Shop\Manager\MealsManager;
use Mums\Shop\Model\Meal;
use Mums\Shop\Model\MealType;
use Mums\Shop\Repository\MealRepository;
use PhpSpec\ObjectBehavior;

class MealsManagerSpec extends ObjectBehavior
{

    public $meal;

    function let(MealRepository $repository)
    {
        $this->beConstructedWith($repository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MealsManager::class);
    }

    function it_give_all_meals(MealRepository $repository)
    {
        $repository->findAll()->willReturn([
            new Meal("ID-1", "meal-1", 11.1, MealType::MAIN),
            new Meal("ID-2", "meal-2", 22.2, MealType::DRINK),
            new Meal("ID-3", "meal-3", 33.3, MealType::DESSERT)
        ]);

        $this->giveMeAll()->shouldHaveCount(3);

    }

    function it_return_those_meals_that_we_want(MealRepository $repository)
    {
        $keys = ["ID-2", "ID-3"];
        $repository
            ->findByIds($keys)
            ->willReturn([
                new Meal("ID-2", "meal-2", 22.2, MealType::DRINK),
                new Meal("ID-3", "meal-3", 33.3, MealType::DESSERT)
        ]);

        $this->find($keys)->shouldHaveCount(2);
    }
}
