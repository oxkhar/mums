<?php

namespace spec\Mums\Shop\Manager;

use Mums\Shop\Manager\CartManager;
use Mums\Shop\Model\Cart;
use Mums\Shop\Repository\CartRepository;
use PhpSpec\ObjectBehavior;

class CartManagerSpec extends ObjectBehavior
{

    function let(CartRepository $repository) {
        $this->beConstructedWith($repository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CartManager::class);
    }

    function it_create_new_cart()
    {
        $this->create()->shouldReturnAnInstanceOf(Cart::class);
    }

    function it_store_cart(CartRepository $repository)
    {
        $cart = new Cart("ID-1");

        $this->store($cart);

        $repository->save($cart)->shouldHaveBeenCalled();
    }

    function it_recover_cart(CartRepository $repository) {

        $id = "ID-1";
        $cart = new Cart($id);

        $repository->findById($id)->willReturn($cart);

        $this->recover($id)->shouldBeEqualTo($cart);
    }


}
