FROM php:5.6-cli

RUN apt-get update && apt-get install -y git \ 
    zip unzip --no-install-recommends \ 
    && pecl install xdebug-2.5.5 \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-install pdo_mysql \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && mkdir /app

WORKDIR /app

