
DROP TABLE IF EXISTS meals;

CREATE TABLE meals (
  uuid varchar(64) NOT NULL UNIQUE,
  name varchar(128) NOT NULL,
  price decima(9,2) NOT NULL,
  type varchar(32) NOT NULL,
  PRIMARY KEY (uuid)
);
