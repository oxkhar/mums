<?php

return [
    'settings' => [
        'displayErrorDetails'    => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        // Renderer settings
        'renderer'               => [
            'template_path' => __DIR__ . '/../templates/',
        ],
    ],
    'meals'    => [
        'meal_repository_class' => Mums\Shop\Repository\Db\MealSqliteRepository::class,
        'meal_manager_class'    => \Mums\Shop\Manager\MealsManager::class,
        'database' => [
            'dsn'      => 'sqlite:' . __APP__ . '/../build/data/meals.db',
            'user'     => null,
            'password' => null,
            'options'  => []

        ]
    ],
    'cart'     => [
        'cart_repository_class' => Mums\Shop\Repository\CartRepository::class,
        'cart_manager_class'    => Mums\Shop\Manager\CartManager::class
    ]
];
