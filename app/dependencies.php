<?php

// DIC configuration
$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};


$container['meals-dbm'] = function ($c) {
    $mealsDb = $c->get('meals')['database'];
    $dbm = new PDO(
        $mealsDb['dsn'],
        $mealsDb['user'],
        $mealsDb['password'],
        $mealsDb['options']
    );
    $dbm->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbm;
};

$container['meals-repository'] = function ($c) {
    $params = $c->get('meals');

    return new $params['meal_repository_class']($c->get('meals-dbm'));
};

$container['meals-manager'] = function ($c) {
    $params = $c->get('meals');

    return new $params['meal_manager_class']($c->get('meals-repository'));
};

return $container;
