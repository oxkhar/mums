<?php

namespace Build;

define("__APP__", __DIR__);

class Install
{

    const SCHEMA_FILE   = __DIR__ . "/db/meals-structure.sql";
    const FIXTURES_FILE = __DIR__ . "/db/meals-fixtures.sql";

    /**
     * @val \PDO
     */
    protected $dbm;

    /**
     * @var \Slim\Container
     */
    protected $container;

    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    public function makeFiles()
    {
        echo "Make build paths..." . PHP_EOL;

        array_map(
            function ($pathDir) {
                is_dir($pathDir) || mkdir($pathDir, 0777, true);
            },
            [
                __DIR__ . "/../build/data",
                __DIR__ . "/../build/tests",
                __DIR__ . "/../build/coverage"
            ]
        );
    }

    public function createDataStructures()
    {
        echo "Create data structures..." . PHP_EOL;

        $this->container->get("meals-dbm")->exec(file_get_contents(static::SCHEMA_FILE));
    }

    public function loadDataFixtures()
    {
        echo "Loading data fixtures for meals" . PHP_EOL;

        $this->container->get("meals-dbm")->exec(file_get_contents(static::FIXTURES_FILE));
    }

    public static function run($di)
    {
        $cmd = new self($di);

        $cmd->makeFiles();
        $cmd->createDataStructures();
        $cmd->loadDataFixtures();
    }
}

$loader = require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/settings.php';

$app = new \Slim\App($settings);

Install::run(require __DIR__ . '/dependencies.php');
