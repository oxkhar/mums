#!/bin/bash

PATH_PWD=$(pwd)
SCRIPT_PATH=$(cd $(dirname $0); pwd)
SCRIPT_NAME=$(basename $0)

echo -e "\n### Iniciando proceso de instalación...\n\n"

CMD_PHP=($(which php5 php))

CMD_COMPOSER=($(which ${SCRIPT_PATH}/bin/composer ${SCRIPT_PATH}/bin/composer.phar composer composer.phar))
if [[ ${CMD_COMPOSER} == "" ]]; then
    $CMD_PHP -r 'readfile("https://getcomposer.org/installer");' | $CMD_PHP -- --install-dir="${SCRIPT_PATH}/bin" --version=1.1.2 --filename=composer
    CMD_COMPOSER="${SCRIPT_PATH}/bin/composer"
fi

$CMD_PHP $CMD_COMPOSER install
EXIT_CODE=$?
if [[ $EXIT_CODE != 0 ]]; then
    echo -e "\n Revisa la configuración de tu sistema para la correcta instalación de la aplicación\n"
    exit $EXIT_CODE
fi

#
echo -e "\n\n### Pasando tests y generando informes...\n"

echo -e "  Comprobando especificaciones...\n"
$CMD_PHP $CMD_COMPOSER tests


echo -e "\n  Generando informes...\n"

$SCRIPT_PATH/bin/phpspec run -c phpspec-coverage.yml 2> /dev/null

IS_OK=$?
if [[ $IS_OK == "0" ]]; then
    echo -e " * (See build/coverage/index.html) * \n"
else
    echo -e " * No fue posible generar informes * \n"
fi
